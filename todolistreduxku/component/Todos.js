import React, { Component } from 'react';
import { Text, View } from 'react-native';
import ToDoItem from './ToDoItem'

export default class Todos extends Component {
    render() {
        return this.props.todos.map((todo) => {
            return (
                <View>
                    <ToDoItem
                        key={i}
                        todo={todo}
                        markComplete={this.props.markComplete}
                        delToDo={this.props.delToDo}
                        updateData={this.props.updateData}
                    />
                </View>
            )
        })
    }
}