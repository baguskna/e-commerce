import axios from 'axios';
export const apiGetUser = token => {
console.log('token nya ', token);
let headers = {
Authorization:`Bearer ${token}`
};
return axios
.get(
'https://app-ecommerceapps.herokuapp.com/users/profile',
{ headers: headers }
)
.then(function(res) {
return res.data.result;
});
};

export const apiAddProduct = token => {
  console.log(token);
  let headers = {
    Authorization: `Bearer ${token}`
  };
  return axios.post('https://app-ecommerceapps.herokuapp.com/products/add', {headers: headers})
  .then(res => {
    return res.data.result;
  });
};