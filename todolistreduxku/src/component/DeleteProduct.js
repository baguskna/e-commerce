import React, { Component } from 'react';
import Axios from 'axios';
import { Text, View, ToastAndroid, Image } from 'react-native';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, TouchableOpacity, Content, Card, CardItem, Right, Footer, FooterTab } from 'native-base';
import { createStackNavigator, createAppContainer, withNavigation } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

class DeleteProduct extends Component {
  constructor(props){
    super(props)
    this.state = {
      data: [],
      name: '',
      productPict: '',
    }
  }
  
  componentDidMount() {
    this.getData()
  }
  async getData() {
    try{
      const getDataItem = async () => await Axios.get('https://app-ecommerceapps.herokuapp.com/products')

      getDataItem()
      .then(result => {
        this.setState({data: result.data.result
      })
      console.log(result.data.result)
    })
      .catch(err => {console.log(err)})
    }
    catch(e) {
      console.log(e)
    }
  }

  async delData(id) {
    try{
      const token = await AsyncStorage.getItem('@token')
      const delItem = async (idItem) => await Axios.delete(`https://app-ecommerceapps.herokuapp.com/products/id/${idItem}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      delItem(id)
      .then(res => {
        this.getData()
        console.log('delete', res.data)
        ToastAndroid.show("Product successfully deleted", ToastAndroid.SHORT)
      })
      .catch(e => {
        ToastAndroid.show("Product deleted failed", ToastAndroid.SHORT)
        console.log(e)
      })
    }
    catch(e) {
      console.log(e)
    }
  }
  
  render() {
    const loops = this.state.data.map(item => {
      return(
        <Card key={item._id}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <CardItem>
              <Left>
                <Body>
                  <Text>{item.name}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{ uri: item.productPict }} style={{ width: 200, height: 200 }} />
            </CardItem>
            <CardItem>
              <Body>
                <Button transparent onPress={() => this.delData(item._id)}>
                  <Text style={{color: 'red', fontWeight: 'bold'}}>Delete</Text>
                </Button>
              </Body>
            </CardItem>
          </View>
        </Card>
      )
    })
    return(
      <Container>
        <Content>
          {loops}
        </Content>
      </Container>
    )
  }
}
export default withNavigation(DeleteProduct);