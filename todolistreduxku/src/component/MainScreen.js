import React, { Component } from 'react'
import { View, StyleSheet, ToastAndroid, Image, TouchableOpacity } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import { Item, Button, Container, Header, Left, Body, Title, Icon, Label, Input, Text, Content, Card, CardItem, Right, Swi, Footer, FooterTab } from 'native-base';
import { createStackNavigator, createAppContainer, withNavigation } from 'react-navigation';


class MainScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      name: '',
      price: '',
      stock: '',
      productPict: '',
    }
  }

  componentDidMount() {
    this.getData()
    //setInterval(this.getData, 1000)
  }
  async getData() {
    try {
      const getDataItem = async () => await axios.get('https://app-ecommerceapps.herokuapp.com/products')

      getDataItem()
        .then(result => {
          this.setState({
            data: result.data.result
          })
          console.log(result.data.result)
        })
        .catch(e => { console.log(e) })
    }
    catch (e) {
      console.log(e)
    }
  }

  // async updateData(id) {
  //   try {
  //     const token = await AsyncStorage.getItem('@token')
  //     const updateDataToDo = async (idd, objParam) => await axios.put(`http://basic-todoapi.herokuapp.com/api/user/todo/update/${idd}`, objParam, { headers: { 'Content-Type': 'application/json', 'Authorization': `jwt1 ${token}` } })

  //     updateDataToDo(id, { status: this.state.status })
  //       .then(response => {
  //         ToastAndroid.show(`Update Data Success`, ToastAndroid.SHORT)
  //         this.getData()
  //         console.log(response)
  //       })
  //       .catch(e => {
  //         ToastAndroid.show("Data Can't be Updated", ToastAndroid.SHORT)
  //         console.log(e)
  //       })
  //   }
  //   catch (e) {
  //     console.log(e)
  //   }
  // }

  async addToCart(id) {
    try {
      const token = await AsyncStorage.getItem('@token')
      const addData = async (bodyParameter) => await axios.post('https://app-ecommerceapps.herokuapp.com/carts', bodyParameter, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      addData({
        product_id: id,
        quantity: '1'
      })
        .then(response => {
          this.getData()
          ToastAndroid.show('Product successfully added to Cart', ToastAndroid.SHORT),
            console.log(response)
        })
        .catch(err => {
          ToastAndroid.show('Product failed added to Cart', ToastAndroid.SHORT),
            console.log('Product failed added to Cart', err)
        })
    }
    catch (e) {
      console.log(e)
    }
  }

  render() {
    const loops = this.state.data.map(item => {
      return (
        <Card key={item._id}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <CardItem>
              <Left>
                <Body>
                  <Text style={styles.textName}>{item.name}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{ uri: item.productPict }} style={{ width: 400, height: 400 }} />
            </CardItem>
            <CardItem>
              <Body>
                <Text style={styles.textPrice}>Rp. {item.price}</Text>
                <View style={{ flexDirection: 'row', flex: 1 }}>
                  <Left>
                    <Text>Stock: {item.stock}</Text>
                  </Left>
                  <Right>
                      <Button rounded style={{ backgroundColor: '#fc8403' }} onPress={() => this.addToCart(item._id)}>
                        <Text>Add To Cart</Text>
                      </Button>
                  </Right>
                </View>
              </Body>
            </CardItem>
          </View>
        </Card>
      );
    })

    return (
      <Container>
        <Content>
          <Header style={{ backgroundColor: '#00acee' }}
            androidStatusBarColor="#00acee">
            <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Item rounded transparent style={styles.searchComponent}>
                <Icon name='ios-search' />
                <Input placeholder='Search' />
              </Item>
            </Body>
          </Header>
          {loops}
        </Content>
        <Footer>
          <FooterTab style={{ backgroundColor: '#00acee' }}>
            <Button>
              <Icon
                style={styles.iconFooter}
                name="home"
                onPress={() => this.props.navigation.navigate('MainScreenm')}
              />
            </Button>
            <Button>
              <Icon
                style={styles.iconFooter}
                name="cart"
                onPress={() => this.props.navigation.navigate('CartScreenm')}
              />
            </Button>
            <Button>
              <Icon
                style={styles.iconFooter}
                name="person"
                onPress={() => this.props.navigation.navigate('ProfileScreenm')}
              />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}
export default withNavigation(MainScreen);

const styles = StyleSheet.create({
  searchComponent: {
    backgroundColor: '#ffff',
    width: 350,
    height: 35
  },
  iconFooter: {
    color: '#ffff',
  },
  textName: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  textPrice: {
    fontWeight: 'bold',
    color: '#fc8403',
    fontWeight: 'bold',
  },
})