import React, { Component } from 'react';
import { Text, View, StyleSheet, ToastAndroid } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Container, Content, Header, Left, Button, Icon, Body, Title, Footer, FooterTab, Card, CardItem, Right } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';

class CartScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      name: '',
      price: '',
      stock: '',
      productPict: '',
    }
  }

  componentDidMount() {
    this.getDataCart()
    //setInterval(this.getDataCart, 1000)
  }
  async getDataCart() {
    const token = await AsyncStorage.getItem('@token')
    console.log('token in cart page', token)
    try {
      const getDataItemCart = async () => await Axios.get('https://app-ecommerceapps.herokuapp.com/carts', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      getDataItemCart()
        .then(result => {
          this.setState({
            data: result.data.result.listCart
          })
          console.log(result)
        })
        .catch(err => {
          console.log(err)
        })
    }
    catch (e) {
      console.log(e)
    }
  }

  async deleteDataCart(id) {
    const token = await AsyncStorage.getItem('@token')
    try {
      const deleteItem = async (idDel) => await Axios.delete(`https://app-ecommerceapps.herokuapp.com/carts/id/${idDel}`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      deleteItem(id)
        .then(res => {
          this.getDataCart()
          console.log('deleted', res)
          ToastAndroid.show('Cart deleted successfully', ToastAndroid.SHORT)
        })
        .catch(err => {
          ToastAndroid.show('Cart deleted failed', ToastAndroid.SHORT)
          console.log('error', err)
        })
    }
    catch (e) {
      console.log(e)
    }
  }

  async checkOutData() {
    const token = await AsyncStorage.getItem('@token')
    console.log(token)
    try {
      const checkOut = await Axios.post('https://app-ecommerceapps.herokuapp.com/orders',null, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      console.log("check out", checkOut)
      // checkOut()
      //   .then(res => {
      //     ToastAndroid.show('Order success', ToastAndroid.SHORT)
      //     console.log("sukses ",res)
      //   })
      //   .catch(err => {
      //     ToastAndroid.show('Order failed', ToastAndroid.SHORT)
      //     console.log("error ",err)
      //   })
      if(checkOut.data.message=="Order Success"){
          ToastAndroid.show('Order success', ToastAndroid.SHORT)
      } else {
          ToastAndroid.show('Order failed', ToastAndroid.SHORT)
      }
    }
    catch (e) {
      console.log("error lagi ",e)
    }
  }

  render() {
    console.log(this.state.data)
    const loops = this.state.data.map((item, i) => {
      return (
        <Card key={i}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <CardItem>
              <Left>
                <Body>
                  <Text>{item.name}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <View style={{ flexDirection: 'row', flex: 1 }}>
                  <Left>
                    <Text>Rp. {item.price}</Text>
                  </Left>
                  <Right>
                    <Button onPress={() => this.deleteDataCart(item._id)}>
                      <Text>Delete</Text>
                    </Button>
                  </Right>
                </View>
              </Body>
            </CardItem>
          </View>
        </Card>
      );
    })

    return (
      <Container>
        <Header style={{ backgroundColor: '#00acee' }}
          androidStatusBarColor="#00acee">
          <Left>
            <Button transparent
              onPress={() => { this.props.navigation.goBack() }}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title style={styles.titleheader}>Cart</Title>
          </Body>
        </Header>
        <Content>
          {loops}
          <Button onPress={() => this.checkOutData()}>
            <Text>Check Out</Text>
          </Button>
        </Content>
      </Container>
    )
  }
}

export default withNavigation(CartScreen);

const styles = StyleSheet.create({
  titleheader: {
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center',
  },
});